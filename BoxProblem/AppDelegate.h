//
//  AppDelegate.h
//  BoxProblem
//
//  Created by sashadiv on 23/12/15.
//  Copyright © 2015 sashadiv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

