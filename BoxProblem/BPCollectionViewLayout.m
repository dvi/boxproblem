//
//  BPCollectionViewLayout.m
//  TestProject
//
//  Created by sashadiv on 23/12/15.
//  Copyright © 2015 sashadiv. All rights reserved.
//

#import "BPCollectionViewLayout.h"

@implementation BPCollectionViewLayout

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSArray *layoutAttributes = [super layoutAttributesForElementsInRect:rect];

    for(int i = 1; i < [layoutAttributes count]; ++i) {
        UICollectionViewLayoutAttributes *currentLayoutAttributes = layoutAttributes[i];
        UICollectionViewLayoutAttributes *prevLayoutAttributes = layoutAttributes[i - 1];
        NSInteger maximumSpacing = 10;
        NSInteger origin = CGRectGetMaxX(prevLayoutAttributes.frame);
        
        if(origin + maximumSpacing + currentLayoutAttributes.frame.size.width < self.collectionViewContentSize.width) {
            CGRect frame = currentLayoutAttributes.frame;
            frame.origin.x = origin + maximumSpacing;
            currentLayoutAttributes.frame = frame;
        }
    }
    return layoutAttributes;
}

@end
